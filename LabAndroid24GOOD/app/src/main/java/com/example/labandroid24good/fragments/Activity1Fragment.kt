package com.example.labandroid24good.fragments

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.example.labandroid24good.R
import com.example.labandroid24good.activities.WFHSessionActivity
import kotlinx.android.synthetic.main.fragment_activity1.*

class Activity1Fragment : Fragment() {

    companion object {
        fun newInstance() = Activity1Fragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view: View=inflater.inflate(R.layout.fragment_activity1, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btn_go_to_session.setOnClickListener() {
            goToSecondActivity()

        }

    }

private fun  goToSecondActivity() {
        val intent= Intent(activity,WFHSessionActivity::class.java)
    startActivity(intent)

    }
}
