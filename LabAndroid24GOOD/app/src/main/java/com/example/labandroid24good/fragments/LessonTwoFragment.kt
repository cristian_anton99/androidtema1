package com.example.labandroid24good.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.labandroid24good.R
import kotlinx.android.synthetic.main.fragment_lesson_two.*

class LessonTwoFragment : Fragment() {

    companion object{
        fun newInstance() : LessonTwoFragment =  LessonTwoFragment()
    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_lesson_two,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Button1.setOnClickListener() {
            button1Function()
        }
        Button2.setOnClickListener() {
            button2Function()
        }
        Button3.setOnClickListener() {
            button3Function()
        }
    }

    private fun  button1Function() {
        val transaction = fragmentManager?.beginTransaction()
        if (transaction != null) {
            transaction.replace(R.id.fly_android_session, LessonThreeFragment(), "Third Fragment")
            transaction.addToBackStack("Third Fragment")
            transaction.commit()
        }
    }

    private fun  button2Function() {
        val transaction = fragmentManager?.beginTransaction()
        if (transaction != null) {
            transaction.remove(LessonOneFragment())
            transaction.commit()
        }
    }

        private fun button3Function() {
            activity?.finish()
        }
}