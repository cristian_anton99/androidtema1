package com.example.labandroid24good.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.example.labandroid24good.R
import kotlinx.android.synthetic.main.fragment_lesson_one.*

class LessonOneFragment : Fragment() {


    companion object{
        fun newInstance() : LessonOneFragment =  LessonOneFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_lesson_one, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btn_trigger.setOnClickListener() {
            goToSecondFragment()
        }
    }

        private fun goToSecondFragment() {
             val transaction = fragmentManager?.beginTransaction()
             if (transaction != null) {
                 transaction.add(R.id.fly_android_session, LessonTwoFragment(), "Second Fragment")
                 transaction.addToBackStack("Second Fragment")
                 transaction.commit()
             }
         }
}
