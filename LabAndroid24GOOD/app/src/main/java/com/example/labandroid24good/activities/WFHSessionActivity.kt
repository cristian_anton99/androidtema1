package com.example.labandroid24good.activities

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.labandroid24good.R
import com.example.labandroid24good.fragments.LessonOneFragment
import com.example.labandroid24good.fragments.LessonTwoFragment
import kotlinx.android.synthetic.main.activity_wfh_session.*

class WFHSessionActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_wfh_session)
        val firstFragment= LessonOneFragment()
      val fr= supportFragmentManager.beginTransaction()
        fr.replace(R.id.fly_android_session,firstFragment)
        fr.commit()
    }


    override fun onBackPressed() {
     val exitIntent=Intent(Intent.ACTION_MAIN)
        exitIntent.addCategory(Intent.CATEGORY_HOME)
        exitIntent.flags=Intent.FLAG_ACTIVITY_CLEAR_TOP
        startActivity(exitIntent)
    }

}
